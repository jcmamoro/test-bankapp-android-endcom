package com.example.testbankapp.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;

public class BAImageView extends androidx.appcompat.widget.AppCompatImageView {
    public BAImageView(Context context) {
        super(context);
    }

    public BAImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BAImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
