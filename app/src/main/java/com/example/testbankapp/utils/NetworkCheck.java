package com.example.testbankapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkCheck {

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean flag = false;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                  flag = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                  flag = true;
            }
        } else {
            flag = false;
        }

        return flag;
    }
}
