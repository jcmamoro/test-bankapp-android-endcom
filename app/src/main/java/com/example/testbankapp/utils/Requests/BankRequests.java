package com.example.testbankapp.utils.Requests;

import com.example.testbankapp.utils.Constants;

import okhttp3.MediaType;
import okhttp3.Request;

public class BankRequests {

    public Request getAccount() {
        Request request = new Request.Builder()
                .url(Constants.BANK_ACCOUNT)
                .build();


        return request;
    }

    public Request getBalance() {
        Request request = new Request.Builder()
                .url(Constants.BANK_BALANCE)
                .build();

        return request;
    }

    public Request getCards() {
        Request request = new Request.Builder()
                .url(Constants.BANK_CARDS)
                .build();

        return request;
    }

    public Request getMovements() {
        Request request = new Request.Builder()
                .url(Constants.BANK_MOVEMENTS)
                .build();

        return request;
    }

}
