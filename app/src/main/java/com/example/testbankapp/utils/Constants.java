package com.example.testbankapp.utils;

public final class Constants {

    public static final String BASE_URL = "http://bankapp.endcom.mx/api/bankappTest";

    public static final String BANK_ACCOUNT = BASE_URL + "/cuenta";
    public static final String BANK_BALANCE = BASE_URL + "/saldos";
    public static final String BANK_CARDS = BASE_URL + "/tarjetas";
    public static final String BANK_MOVEMENTS = BASE_URL + "/movimientos";
}
