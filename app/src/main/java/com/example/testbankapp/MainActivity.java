package com.example.testbankapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toolbar;

import com.example.testbankapp.adapters.AccountAdapter;

import com.example.testbankapp.adapters.CardAdapter;
import com.example.testbankapp.adapters.MovementsAdapter;
import com.example.testbankapp.databinding.ActivityMainBinding;
import com.example.testbankapp.models.AccountModel;
import com.example.testbankapp.models.BalanceModel;
import com.example.testbankapp.models.CardModel;
import com.example.testbankapp.models.MovementModel;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private BankRepository mBankRepository = new BankRepository();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.textAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AssociateCardActivity.class);
                startActivity(intent);
            }
        });

        mBankRepository.setOnBankResponse(new BankRepository.OnBankResponse() {
            @Override
            public void onGetAccount(AccountModel model) {
                Log.i("MODEL_ACCOUNT", model.cuenta.get(0).nombre );
                binding.textName.setText( model.cuenta.get(0).nombre);
                binding.textSession.setText( model.cuenta.get(0).ultimaSesion );
            }

            @Override
            public void onGetBalance(BalanceModel model) {
                Log.i("MODEL_BALANCE", model.saldos.toString());
                binding.recyclerView.setAdapter( new AccountAdapter(model.saldos) );
            }

            @Override
            public void onGetCards(CardModel model) {
                Log.i("MODEL_CARDS", model.tarjetas.toString());
                binding.recyclerView2.setAdapter( new CardAdapter( model.tarjetas ));
            }

            @Override
            public void onGetMovements(MovementModel model) {
                Log.i("MODEL_MOVEMENTS", model.movimientos.toString());
                binding.recyclerView3.setAdapter( new MovementsAdapter( model.movimientos, getApplicationContext() )  );
            }
        });

        mBankRepository.getAccount();
        mBankRepository.getBalance();
        mBankRepository.getCards();
        mBankRepository.getMovements();
    }


}