package com.example.testbankapp.models;

/*{
        "tarjetas": [
            {
            "tarjeta": "5439240112341234",
            "nombre": "Berenice Garcia Lopez",
            "saldo": 1500,
            "estado": "activa",
            "tipo": "titular",
            "id": 2
            },
            {
            "tarjeta": "5439240112341235",
            "nombre": "Lilly Silva Martinez",
            "saldo": 500,
            "estado": "desactivada",
            "tipo": "titular",
            "id": 3
            }
        ]
}*/

import java.util.List;

public class CardModel {

    public List<Tarjeta> tarjetas;

    public static class Tarjeta {
        public String tarjeta;
        public String nombre;
        public String saldo;
        public String estado;
        public String tipo;
        public int id;

    }

}
