package com.example.testbankapp.models;


/*
    "cuenta": [
            {
            "cuenta": 19032723782,
            "nombre": "Octavio Martinez",
            "ultimaSesion": "07/07/2019 20:10:55",
            "id": 2
            }
            ]
 */

import java.util.List;

public class AccountModel {

    public List<Cuenta> cuenta;

    public static class Cuenta {
        public String cuenta;
        public String nombre;
        public String ultimaSesion;
        public int id;

    }

}
