package com.example.testbankapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;


import com.example.testbankapp.databinding.ActivityAssociateCard2Binding;
import com.google.gson.Gson;


public class AssociateCardActivity extends AppCompatActivity {

    private ActivityAssociateCard2Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAssociateCard2Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.btnAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getJson();

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage(message)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                builder.create();
                builder.show();
            }
        });
    }

    private String getJson() {
        Card card = new Card();
        card.number = String.valueOf(binding.editCardNumber.getText());
        card.account = String.valueOf(binding.editAccount.getText());
        card.issure = String.valueOf(binding.editIssure.getText());
        card.name = String.valueOf(binding.editCardName.getText());
        card.brand = String.valueOf(binding.editBrand.getText());
        card.status = String.valueOf(binding.editCardStatus.getText());
        card.balance = String.valueOf(binding.editBalance.getText());
        card.accountType = String.valueOf(binding.editAccountType.getText());

        return new Gson().toJson(card);
    }

    public class Card {
        public String number = "";
        public String account = "";
        public String issure = "";
        public String name = "";
        public String brand = "";
        public String status = "";
        public String balance = "";
        public String accountType = "";
    }
}