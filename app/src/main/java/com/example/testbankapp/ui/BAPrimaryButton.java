package com.example.testbankapp.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class BAPrimaryButton extends androidx.appcompat.widget.AppCompatButton {
    public BAPrimaryButton(Context context) {
        super(context);
    }

    public BAPrimaryButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BAPrimaryButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
