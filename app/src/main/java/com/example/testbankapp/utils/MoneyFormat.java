package com.example.testbankapp.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class MoneyFormat {

    public String format(String price) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');

        DecimalFormat df = new DecimalFormat("#,###.00", otherSymbols);

        return "$" + df.format( Double.parseDouble(price) );


    }

}
