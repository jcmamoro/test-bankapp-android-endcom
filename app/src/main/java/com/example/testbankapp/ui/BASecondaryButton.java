package com.example.testbankapp.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class BASecondaryButton extends androidx.appcompat.widget.AppCompatButton {
    public BASecondaryButton(Context context) {
        super(context);
    }

    public BASecondaryButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BASecondaryButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
