package com.example.testbankapp.models;
/*
    {
        "saldos": [
        {
        "cuenta": 19032723782,
        "saldoGeneral": 2000,
        "ingresos": 8000,
        "gastos": 6000,
        "id": 2
        }
        ]

       }
 */

import java.util.List;

public class BalanceModel {

    public List<Saldo> saldos;

    public static class Saldo {
        public String cuenta;
        public String saldoGeneral;
        public String ingresos;
        public String gastos;
        public int id;

    }


}
