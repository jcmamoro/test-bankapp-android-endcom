package com.example.testbankapp.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;



public class Messages {

    private Context mContext;
    public ProgressDialog mProgressDialog;

    public Messages(Context context) {
        this.mContext = context;
        createProgressDialog();
    }

    public void showErrorMessage( String title, String message ){
        AlertDialog.Builder builder = new AlertDialog.Builder( mContext );
        builder.setTitle(title);
        builder.setMessage( message );
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createProgressDialog( ) {
        mProgressDialog = new ProgressDialog( mContext );
        mProgressDialog.setCancelable(false);
    }

    public void showProgressDialog(String message)
    {
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }



}
