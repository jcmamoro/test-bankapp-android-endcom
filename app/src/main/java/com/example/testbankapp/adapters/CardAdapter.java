package com.example.testbankapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testbankapp.R;
import com.example.testbankapp.models.CardModel;
import com.example.testbankapp.utils.MoneyFormat;

import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder>{

    private List<CardModel.Tarjeta> mList;

    public CardAdapter(List<CardModel.Tarjeta> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_card_item, parent,false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MoneyFormat moneyFormat = new MoneyFormat();
        CardModel.Tarjeta card = mList.get(position);
        holder.textCardStatus.setText( card.estado );
        holder.textCardNumber.setText( card.tarjeta );
        holder.textCardClientName.setText( card.nombre );
        holder.textCardType.setText( card.tipo );

        String stringBalance = moneyFormat.format( card.saldo );
        holder.textCardBalance.setText( stringBalance );

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textCardStatus,
                textCardNumber,
                textCardClientName,
                textCardType,
                textCardBalance;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textCardStatus = itemView.findViewById(R.id.text_card_status);
            textCardNumber = itemView.findViewById(R.id.text_card_number);
            textCardClientName = itemView.findViewById(R.id.text_card_client_name);
            textCardType = itemView.findViewById(R.id.text_card_type);
            textCardBalance = itemView.findViewById(R.id.text_card_balance);

        }
    }
}
