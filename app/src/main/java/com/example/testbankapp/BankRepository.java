package com.example.testbankapp;

import android.accounts.Account;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.testbankapp.models.AccountModel;
import com.example.testbankapp.models.BalanceModel;
import com.example.testbankapp.models.CardModel;
import com.example.testbankapp.models.MovementModel;
import com.example.testbankapp.utils.Requests.BankRequests;
import com.google.gson.Gson;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class BankRepository extends ViewModel {

    private BankRequests mBankRequest = new BankRequests();
    private Gson mGson = new Gson();
    private OnBankResponse mOnBankResponse;

    public void getAccount() {
        ExecuteWebService webService = (ExecuteWebService) new ExecuteWebService(mBankRequest.getAccount());
        webService.setOnExecuteWebService(new OnExecuteWebService() {
            @Override
            public void onPostExecute(String response) {
                if (response != null) {
                    Log.i("BANK_ACCOUNT", response);
                    mOnBankResponse.onGetAccount( mGson.fromJson(response, AccountModel.class) );
                }
            }
        });
        webService.execute();
    }

    public void getBalance() {
        ExecuteWebService webService = (ExecuteWebService) new ExecuteWebService(mBankRequest.getBalance());
        webService.setOnExecuteWebService(new OnExecuteWebService() {
            @Override
            public void onPostExecute(String response) {
                if (response != null) {
                    Log.i("BANK_ACCOUNT", response);
                    mOnBankResponse.onGetBalance( mGson.fromJson(response, BalanceModel.class) );
                }

            }
        });
        webService.execute();
    }

    public void getCards() {
        ExecuteWebService webService = (ExecuteWebService) new ExecuteWebService(mBankRequest.getCards());
        webService.setOnExecuteWebService(new OnExecuteWebService() {
            @Override
            public void onPostExecute(String response) {
                if (response != null) {
                    Log.i("BANK_ACCOUNT", response);
                    mOnBankResponse.onGetCards( mGson.fromJson(response, CardModel.class) );
                }
            }
        });
        webService.execute();
    }

    public void getMovements() {
        ExecuteWebService webService = (ExecuteWebService) new ExecuteWebService(mBankRequest.getMovements());
        webService.setOnExecuteWebService(new OnExecuteWebService() {
            @Override
            public void onPostExecute(String response) {
                if (response != null) {
                    Log.i("BANK_ACCOUNT", response);
                    mOnBankResponse.onGetMovements( mGson.fromJson(response, MovementModel.class) );
                }
            }
        });
        webService.execute();
    }

    public class ExecuteWebService extends AsyncTask<String,String, String> {

        OkHttpClient client = new OkHttpClient();

        private Request mRequest;
        private OnExecuteWebService mListener;


        public ExecuteWebService(Request mRequest) {
            this.mRequest = mRequest;

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Response response = client.newCall(mRequest).execute();
                return Objects.requireNonNull(response.body()).string();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            mListener.onPostExecute(response);
        }

        public void setOnExecuteWebService(OnExecuteWebService listener) {
            this.mListener = listener;
        }

    }

    public interface OnExecuteWebService {
        void onPostExecute(String response);
    }

    public interface OnBankResponse {
        void onGetAccount(AccountModel model);
        void onGetBalance(BalanceModel model);
        void onGetCards(CardModel model);
        void onGetMovements(MovementModel model);
    }

    public void setOnBankResponse(OnBankResponse listener) {
        this.mOnBankResponse = listener;
    }

}
