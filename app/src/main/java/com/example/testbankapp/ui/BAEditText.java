package com.example.testbankapp.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class BAEditText extends androidx.appcompat.widget.AppCompatEditText {
    public BAEditText(Context context) {
        super(context);
    }

    public BAEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BAEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
