package com.example.testbankapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.testbankapp.R;
import com.example.testbankapp.databinding.RecyclerAccountItemBinding;
import com.example.testbankapp.models.BalanceModel;
import com.example.testbankapp.utils.MoneyFormat;

import java.util.List;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ViewHolder> {

    private List<BalanceModel.Saldo> mList;

    public AccountAdapter(List<BalanceModel.Saldo> list) {
        this.mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_account_item, parent,false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BalanceModel.Saldo balance = mList.get(position);
        MoneyFormat moneyFormat = new MoneyFormat();
        String stringBalance = moneyFormat.format( balance.saldoGeneral );
        holder.textBalance.setText( stringBalance );
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textBalance;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textBalance = itemView.findViewById(R.id.text_balance);
        }
    }
}
