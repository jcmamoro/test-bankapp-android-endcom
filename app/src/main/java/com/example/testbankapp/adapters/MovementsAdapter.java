package com.example.testbankapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testbankapp.R;
import com.example.testbankapp.models.MovementModel;
import com.example.testbankapp.utils.MoneyFormat;

import java.util.List;

public class MovementsAdapter extends RecyclerView.Adapter<MovementsAdapter.ViewHolder> {

    private Context mContext;
    private List<MovementModel.Movimiento> mList;
    private static final String MOVEMENT_PAYMENT = "abono";

    public MovementsAdapter(List<MovementModel.Movimiento> mList, Context context) {
        this.mList = mList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MovementsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_movement_item, parent,false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovementsAdapter.ViewHolder holder, int position) {
        MoneyFormat moneyFormat = new MoneyFormat();
        MovementModel.Movimiento movement = mList.get(position);
        holder.textType.setText( movement.tipo );
        holder.textDate.setText( movement.fecha );

        String stringAmount = moneyFormat.format( movement.monto );
        holder.textAmount.setText( stringAmount  );

        if ( movement.tipo.equals(MOVEMENT_PAYMENT) ) {
            holder.textAmount.setTextColor( mContext.getResources().getColor(R.color.success)  );
        } else {
            holder.textAmount.setTextColor( mContext.getResources().getColor(R.color.danger)  );
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textType, textDate, textAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textType = itemView.findViewById(R.id.text_movement_type);
            textDate = itemView.findViewById(R.id.text_movement_date);
            textAmount = itemView.findViewById(R.id.text_movement_amount);
        }
    }
}
