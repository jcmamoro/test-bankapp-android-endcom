package com.example.testbankapp.models;

/*{
        "movimientos": [
            {
            "fecha": "01/07/2019",
            "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "monto": "30.00",
            "tipo": "abono",
            "id": 2
            },
        ]
        }*/

import java.util.List;

public class MovementModel {

    public List<Movimiento> movimientos;

    public static class Movimiento {
        public String fecha;
        public String descripcion;
        public String monto;
        public String tipo;
        public int id;

    }

}
